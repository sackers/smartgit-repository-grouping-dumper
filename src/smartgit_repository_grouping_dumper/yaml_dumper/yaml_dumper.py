import smartgit_repository_grouping_dumper.smartgit.types as types
import yaml


def data_representer(
    dumper: yaml.SafeDumper, item: types.YamlRepresentable
) -> yaml.nodes.MappingNode:
    return dumper.represent_data(item.yaml_representation())


def get_dumper():
    """Add representers to a YAML seriailizer."""
    safe_dumper = yaml.SafeDumper
    safe_dumper.add_representer(types.Group, data_representer)
    safe_dumper.add_representer(types.Repository, data_representer)
    safe_dumper.add_representer(types.SmartGitRepositoryGrouping, data_representer)
    return safe_dumper
