import os
from typing import Iterable


def is_git_repo(path: str) -> bool:
    return os.path.isdir(os.path.join(path, ".git"))


def find_git_repos(path: str) -> Iterable[str]:
    for child in os.scandir(path):
        if child.is_dir():
            if is_git_repo(child):
                yield child
            else:
                yield from find_git_repos(child)
