import argparse
import os

import smartgit_repository_grouping_dumper.smartgit.types as types
import yaml
from smartgit_repository_grouping_dumper.git.find_git_repos import find_git_repos
from smartgit_repository_grouping_dumper.yaml_dumper.yaml_dumper import get_dumper


def append_to_tree(tree, path, item):
    if not path:
        return
    if path not in tree:
        new_grp = types.Group(name=path[-1], children=[item])
        tree[path] = new_grp
        append_to_tree(tree, path[:-1], new_grp)
    else:
        tree[path].children.append(item)


def build_structure(search_path: str):
    repos = find_git_repos(search_path)
    groups = dict()

    for repo in repos:
        parent = tuple(repo.path.removeprefix(search_path).split("/")[1:-1])
        r = types.Repository.from_dir_entry(repo)
        append_to_tree(tree=groups, path=parent, item=r)

    root_dict = {k: v for k, v in groups.items() if len(k) == 1}

    structure = types.SmartGitRepositoryGrouping(
        structure=list(root_dict.values()), recently_used=[], ignored=[]
    )

    print(
        yaml.dump(
            structure, default_flow_style=None, Dumper=get_dumper(), sort_keys=False
        )
    )


def main():
    parser = argparse.ArgumentParser(
        description="Creates SmartGits repository-grouping.yaml file by searching for git repos from a root path",
    )
    parser.add_argument(
        "-p", "--path", help="Root path to start scanning", type=str, required=True
    )
    args = parser.parse_args()
    build_structure(search_path=args.path)


if __name__ == "__main__":
    main()
