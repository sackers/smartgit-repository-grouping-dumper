from dataclasses import dataclass
from typing import List
import abc


@dataclass
class YamlRepresentable(abc.ABC):
    @abc.abstractmethod
    def yaml_representation(self):
        pass


@dataclass
class SmartGitRepositoryGrouping(YamlRepresentable):
    structure: List
    recently_used: List
    ignored: List

    def yaml_representation(self):
        return {
            "structure": self.structure,
            "recentlyUsed": self.recently_used,
            "ignored": self.ignored,
        }


@dataclass
class Group(YamlRepresentable):
    name: str
    children: List["Group"]

    @classmethod
    def from_dir_entry(cls, dir_entry):
        return cls(name=dir_entry.name, children=[])

    def yaml_representation(self):
        return {
            "name": self.name,
            "children": self.children,
        }


@dataclass
class Repository(YamlRepresentable):
    name: str
    path: str
    absolute_path: str

    @classmethod
    def from_dir_entry(cls, dir_entry):
        return cls(
            name=dir_entry.name, path=dir_entry.path, absolute_path=dir_entry.path
        )

    def yaml_representation(self):
        return {
            "name": self.name,
            "path": self.path,
            "path.absolute": self.absolute_path,
        }
