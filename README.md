# SmartGit Repository-Grouping Dumper

`SmartGit` stores the `Repository View` in
`~/.config/smartgit/22.1/repository-grouping.yml`. This file contains among
other things the repositories and their groups.

Since `SmartGit` does not provide a function to create the groups automatically
it's very time consuming to create the groupes, based on the directory
structure, by hand..

This project dumps a directory tree into the required structure of SmartGits
`repository-grouping.yml` file.



## Build

Clone the repository and build a distribution by either using the legacy
setuptools way

```shell
$ python setup.py bdist_wheel
```

or by using [build](https://pypi.org/project/build/)

```shell
$ python -m build
```

or [pip](https://pypi.org/project/pip/)

```shell
$ pip wheel .
```

## Install

Via pip:
```shell
$ pip install git+https://gitlab.com/sackers/smartgit-repository-grouping-dumper
# or
$ pip install <path to cloned repository>
# or
$ pip install <path to wheel>
```

## Usage

### Getting Help

```shell
$ create_repo_grouping --help
```

### Dump directory tree

If all your git repositories are stored at `~/repos` and `SmartGit` stores its
config at `~/.config/smartgit/22.1/`:

```shell
$ create_repo_grouping --path ~/repos > ~/.config/smartgit/22.1/repository-grouping.yml
```
